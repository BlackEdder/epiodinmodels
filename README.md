# epiOdinModels

Collection of my epidemiological ODE models developed using the 'mrc-ide/odin' package. Models are mainly aimed at influenza and influenza like diseases. These models are in a separate package partly because odin and Rcpp do not always get along.
